#include <IRremote.h>
#include <LiquidCrystal.h>
#define IR 6
#define MOV 8
#define LUZ A0
#define BUZ 7

IRrecv receptor(IR);
decode_results resultado;
bool ligado = false;
bool senslumi = false;
int luminosidade = 0;

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  lcd.begin(16, 2);
  receptor.enableIRIn();
  Serial.begin(9600);
  pinMode(MOV,INPUT);
  pinMode(BUZ,OUTPUT);
}

void loop() {
  
  ligadooudesligado();
  modoligado();
  modoluz();
  alarme();
  luminosida();
  Serial.println(resultado.value);
  Serial.println(senslumi);
}

void ligadooudesligado(){
	if(receptor.decode(&resultado)){
    if(resultado.value == 0xFD00FF){
      ligado = !ligado;
    	senslumi = false;
    }
    if(resultado.value == 16597183){
    	senslumi = !senslumi;
    }
    receptor.resume();
  }
  }


void modoligado(){
  if(ligado == true){lcd.setCursor(0,0);
  lcd.write('A');
  lcd.setCursor(1,0);
  lcd.write('l');
  lcd.setCursor(2,0);
  lcd.write('a');
  lcd.setCursor(3,0);
  lcd.write('r');
  lcd.setCursor(4,0);
  lcd.write('m');
  lcd.setCursor(5,0);
  lcd.write('e');
  lcd.setCursor(6,0);
  lcd.write(' ');
  lcd.setCursor(7,0);
  lcd.write('O');
  lcd.setCursor(8,0);
  lcd.write('N');
  lcd.setCursor(9,0);
  lcd.write(' ');  
            }
  if(ligado == false){
  lcd.setCursor(0,0);	
  lcd.write('A');
  lcd.setCursor(1,0);
  lcd.write('l');
  lcd.setCursor(2,0);
  lcd.write('a');
  lcd.setCursor(3,0);
  lcd.write('r');
  lcd.setCursor(4,0);
  lcd.write('m');
  lcd.setCursor(5,0);
  lcd.write('e');
  lcd.setCursor(6,0);
  lcd.write(' ');
  lcd.setCursor(7,0);
  lcd.write('O');
  lcd.setCursor(8,0);
  lcd.write('F');
  lcd.setCursor(9,0);
  lcd.write('F');
  }
}

void alarme(){
  if(ligado == true && digitalRead(MOV) == 1){
  	digitalWrite(BUZ,0);
    delay(100);
    digitalWrite(BUZ,1);
    delay(100);
  }
  if(ligado == false){
  	digitalWrite(BUZ,0);
  }
}

void luminosida(){
  if(senslumi == true){
    luminosidade = analogRead(LUZ);
    if(luminosidade > 115){
    	ligado = true;
    }else{
    	ligado = false;
    }
  }
}

void modoluz(){
  if(senslumi == true){lcd.setCursor(0,1);
  lcd.write('M');
  lcd.setCursor(1,1);
  lcd.write('o');
  lcd.setCursor(2,1);
  lcd.write('d');
  lcd.setCursor(3,1);
  lcd.write('o');
  lcd.setCursor(4,1);
  lcd.write(' ');
  lcd.setCursor(5,1);
  lcd.write('L');
  lcd.setCursor(6,1);
  lcd.write('u');
  lcd.setCursor(7,1);
  lcd.write('z');
  lcd.setCursor(8,1);
  lcd.write(' ');
  lcd.setCursor(9,1);
  lcd.write('O');
  lcd.setCursor(10,1);
  lcd.write('N');
  lcd.setCursor(11,1);
  lcd.write(' ');
            }
  if(senslumi == false){
  lcd.setCursor(0,1);	
  lcd.write('M');
  lcd.setCursor(1,1);
  lcd.write('o');
  lcd.setCursor(2,1);
  lcd.write('d');
  lcd.setCursor(3,1);
  lcd.write('o');
  lcd.setCursor(4,1);
  lcd.write(' ');
  lcd.setCursor(5,1);
  lcd.write('L');
  lcd.setCursor(6,1);
  lcd.write('u');
  lcd.setCursor(7,1);
  lcd.write('z');
  lcd.setCursor(8,1);
  lcd.write(' ');
  lcd.setCursor(9,1);
  lcd.write('O');
  lcd.setCursor(10,1);
  lcd.write('F');
  lcd.setCursor(11,1);
  lcd.write('F');
  }
}
