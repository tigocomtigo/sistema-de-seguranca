# Alarme com sensor de luminosidade

Desenvolvido por Thiago Gonçalves Salustiano. Estudante do curso de engenharia da computação na UTFPR - Pato Branco.

## Objetivo

Um alarme simples, com um sensor de luminosidade e um monitor lcd. Ele pode ser ativado e desativado com um controle remoto.

## Circuito

[Clique aqui para visualizar o projeto no Tinkercad!](https://www.tinkercad.com/things/aXqI4Nz4dJT-shiny-borwo/editel?sharecode=0eqHQ2itfK_u3vQilAfGy9SmM1-K9cT1KWtVGtTGD5w)

![](img/projeto.png)

## Funcionamento

Para ligar o alarme você deve apertar no botão "ON" do controle remoto, para ligar o sensor de luminosidade aperte o botão "FUNC/STOP". Se o alarme estiver ligado e o sensor de movimento detectar uma movimentação, ele irá ativar o piezo disparando o alarme. O modo luz tem a função de ligar automáticamente o alarme quando determinada luminosidade for atingida.

![](img/imagem_ligado.png)

## Links Úteis

[Tinkercad](https://www.tinkercad.com/)

